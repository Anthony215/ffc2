<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // View::composer(['layouts.appCart', 'product.index'], function ($view) {
        //     $view->with([
        //         'cartCount' => Cart::getTotalQuantity(),
        //         'cartTotal' => Cart::getTotal(),
        //     ]);
        // });
    }
}
