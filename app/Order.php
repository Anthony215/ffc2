<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
     /**
     * the attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'qty', 'totalPrice'
    ];
    
}