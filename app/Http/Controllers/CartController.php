<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $content = Cart::getContent();
        // $total = Cart::getTotal();

        // return view('/viewProduct/cart', compact('content', 'total'));

        return view('/viewProduct/cart');
    }
    /**
     * function store
     */
    public function store(Request $request)
    {
        
        // $duplicata = Cart::search(function ($cartItem, $rowId) use ($request) {
        //     return $cartItem->id == $request->product_id;
        // });
        // if($duplicata->isNotEmpty()) {
        //     return redirect()->route('cart.index')->with('success', 'l\'article a déjà été ajouté!');
        // }//methode pour ne pas afficher 2 fois le même article dans le panier
        //dd($request->id, $request->name, $request->price);

        // $product = Product::find($request->prodcut_id);
        // Cart::add($product->id, $product->name, 1, $product->price)
        // ->associate('App\Product');

        // return redirect()->route('cart.index')->with('success', 'Produit ajouté au panier');
        //dd($request->id, $request->name, $request->price);
        Cart::add($request->id, $request->name, 1, $request->price)
        ->associate('App\Product');

        return back()->with('success', 'Produit ajouté au panier!');
       
    }
    public function update(Request $request, $id)
    {
        Cart::update($id, [
            'quantity' => ['relative' =>false, 'value' => $requesr->quantity],
        ]);
        return redirect(route('cart.index'));
    }
    /**
     * function remove
     */
    public function destroy($rowId)
    {
        Cart::remove($rowId);

        return back()->with('success_message', 'panier vidé!');

    }
    
}