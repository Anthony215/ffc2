<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $user = User::get();
        return view('/viewUser/listUsers', compact('user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::pluck('name', 'name')->all();
        return view('createUsers', compact('roles'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstName'=>'required',
            'name'=>'required',
            'phoneNumber'=>'required',
            'address'=>'required',
            'email'=>'required|email|unique:users, email',
            'password'=>'required| same: confirm-password',
            'roles' =>'required'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        User::create($input);
        $user->assignRole($request->input('roles'));

        return redirect()->route('formulaire')
            ->with('User', '$user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $user = User::get();
        return view('/viewUser/updateCompte', compact ('user'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('/viewUser/updateUser', compact('user', 'roles', 'userRole'));
        // $user = User::get();
        // return view('/viewUser/updateUser', compact('user'));
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        //dd($request->all());
        // $user->update($request->all());
        // return redirect()->route('user.index')->with('success', 'Compte modifié');
        
            $user->firstName = request('firstName');
            $user->name = request('name');
            $user->phoneNumber = request('phoneNumber');
            $user->address = request('address');
            $user->email = request('email');
            $user->password = Hash::make($request['password']);
            $user->save();
        
        return redirect()->route('user.index')->with('success', 'Compte modifié');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('home')
        ->with('success', 'User deleted with success');
    }
    
    /**
     * method to display a test page and to create a virgin page
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['destroy','update']);
        
    }
     /**
     * display form
     */
    // public function formulaire(Request $request)
    // {

    //     $user = new App\User;
    //     $user->firstName = request('firstName');
    //     $user->name = request('name');
    //     $user->phoneNumber = request('phoneNumber');
    //     $user->address = request('address');
    //     $user->email = request('email');
    //     $user->password = bcrypt(request('password'));

    //     $user->save();
    //     //dump(request());
    //     return view('/viewUser/formulaireOK', compact('user'));

    // }

}