<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class DrinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::get()->whereBetween('id',[7, 11]);
        return view('viewProduct/products')->with('products', $products);//vue boissons
    }
    public function show()
    {
        $products = Product::get()->whereBetween('id',[16, 18]);
        return view('viewProduct/products')->with('products', $products);//vue sauces
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $products = Product::get()->whereBetween('id',[19, 21]);
        return view('viewProduct/menu')->with('products', $products);//vue menus
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
