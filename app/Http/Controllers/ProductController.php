<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class ProductController extends Controller
{
    public function index()
    {
        //dd(Cart::content());
        $products = Product::get()->take(6);
        
        return view('viewProduct/products')->with('products', $products);//vue burger
    }
    public function store(Request $request)
    {
            $this->validate($request,[
            'name' => 'required',
            'detail' => 'required',
            'price' => 'required'
            // 'uuid' => 'required'
            ]);

            $products = $request->all();
            
    
            Product::create($input);
        
            return redirect()->route('product')
                ->with('Products', '$products');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $product = Product::find($id);
        // return view('/viewProduct/burgers', compact('product'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $products = Product::get()->whereBetween('id',[12, 15]);
        return view('viewProduct/products')->with('products', $products);//vue accompagnements
    }
}
