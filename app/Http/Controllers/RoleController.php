<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * display a listing of the resource
     */
    function __construct()
    {
        $this->middleware('permission:index');
        $this->middleware('permission:create');
        $this->middleware('permission:store');
        $this->middleware('permission:update');
        $this->middleware('permission:show');
        $this->middleware('permission:edit');
        $this->middleware('permission:destroy');

        $this->middleware(['role:admin', 'permission:index|create|store|update|show|edit|destroy']);
        $this->middleware(['role:customer', 'permission:index|create|store|update']);

    }
    /**
     * display a listing of the resource
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id', 'DESC')->paginate(5);
        return view('roles.index', compact('roles'))
        ->with('i', ($request->input('page', 1)-1)*5);
    }
    /**
     * show the form for creating resource in storage
     */
    public function create()
    {
        $permission = Permission::get();
        return view('roles.create', compact('permission'));
    }
    /**
     * store function
     */
    public function store(Request $request)
    {
        // if($request->user()->can('store-tasks')) {
            $this->validate($request, [
                'name' => 'required|unique::roles, name',
                'permission' => 'required',
            ]);
    
            $role = Role::create(['name' => $request->input('name')]);
            $role->syncPemrission($request->input('permission'));
    
            return redirect()->route('roles.index')
            ->with('success', 'Role created with success');
        
        
    }
    /**
     * display the specified resource
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "permissions.id")
        ->where("role_has_permissions.role_id", $id)
        ->get();

        return view('roles.show', compact('role', 'rolePermissions'));
    }
    /**
     * show the form for editing the specified resource
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
        ->pluck('role_has_peremissions.permission_id', 'role_has_permissions.permission_id')
        ->all();

        return view('roles.edit', compact('role', 'permission', 'rolePermissions'));
    }
    /**
     * update the specified resource in storage
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'permission' => 'required',
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')
        ->with('success', 'Role updated successfully');
    }
    /**
     * remove the specified resource
     */
    public function destroy($id)
    {
        if($request->user()->can('delete-tasks')) {
            DB::table("roles")->where('id', $id)->delete();
        return redirect()->route('roles.index')
        ->with('success', 'Role deleted successfully');
        }
        
    }
}
