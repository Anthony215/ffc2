<?php

namespace App;


use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    /**
     * the attributes that are mass assignable.
     * 
     * @var array
     */
   
    protected $fillable = [
        'name', 'detail', 'price'
    ];


    // public function getPrice()
    // {
    //     $price = $this->price / 100;

    //     return number_format($price, 2, ',', ' ').'€';
    // }

}