<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'classique',
                'detail' => 'steak grillé, tomates, oignons, salade et sauce maison',
                'price' => '6.65',
                //'uuid' => '1'
            ],
            [
                'name' => 'double chicken',
                'detail' => 'burger double tenders de poulet, oignons rouge, fromage et salade',
                'price' => '6.55',
                //'uuid' => '2'
            ],
            [
                'name' => 'double fish',
                'detail' => 'double petit burger de poisson avec sa sauce maison',
                'price' => '5.90',
                //'uuid' => '3'
            ],
            [
                'name' => 'double cheese',
                'detail' => 'burger double steaks et fromage à raclette',
                'price' => '7.90',
                //'uuid' => '4'
            ],
            [
                'name' => 'veggie',
                'detail' => 'burger steak de haricots et lentilles, sauce tomate et oignons',
                'price' => '6.10',
                //'uuid' => '5'
            ],
            [
                'name' => 'burger child',
                'detail' => 'burger steak, salade et fromage',
                'price' => '4.20',
            ],
            [
                'name' => 'Coca Cola',
                'detail' => 'soda frais avec ou sans glace sur demande',
                'price' => '2.90',
            ],
            [
                'name' => 'Vittel',
                'detail' => 'eau plate fraîche bouteille',
                'price' => '1.20',
            ],
            [
                'name' => 'Fanta',
                'detail' => 'orange',
                'price' => '2.40',
            ],
            [
                'name' => 'Ice Tea',
                'detail' => 'thé glacé à la pêche',
                'price' => '2.20',
            ],
            [
                'name' => 'St Yorre',
                'detail' => 'eau gazeuse naturelle',
                'price' => '2.40',
            ],
            [
                'name' => 'Frite',
                'detail' => '100g',
                'price' => '3.20',
            ],
            [
                'name' => 'Big Frite',
                'detail' => '180g',
                'price' => '4.40',
            ],
            [
                'name' => 'Potatoes',
                'detail' => 'pomme de terre frites, 120g',
                'price' => '4.00',
            ],
            [
                'name' => 'Salade',
                'detail' => 'salade verte accompagnée de sauce individuelle',
                'price' => '2.00',
            ],
            [
                'name' => 'Sauce mayonnaise',
                'detail' => 'portion sauce mayonnaise',
                'price' => '1.50',
            ],
            [
                'name' => 'Ketchup',
                'detail' => 'portion sauce ketchup',
                'price' => '1.50',
            ],
            [
            'name' => 'barbecue',
                'detail' => 'portion sauce barbecue',
                'price' => '1.50',
            ],
            [
                'name' => 'Menu classique',
                    'detail' => 'Un burger, son accompagnement et une boisson',
                    'price' => '9.80',
            ],
            [
                'name' => 'Menu extra',
                    'detail' => 'Un double burger, son accompagnement et une boisson',
                    'price' => '11.50',
            ],
            [
                'name' => 'Menu Petit Loup',
                    'detail' => 'Un burger enfant, son accompagnement et une boisson',
                    'price' => '5.50',
            ],
        ]);

    }
}
