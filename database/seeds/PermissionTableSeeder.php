<?php

use App\Permission;
//use Spatie\Permission\Model\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create permissions
        Permission::create(['name' => 'index']);
        Permission::create(['name' => 'create']);
        Permission::create(['name' => 'store']);
        Permission::create(['name' => 'show']);
        Permission::create(['name' => 'edit']);
        Permission::create(['name' => 'update']);
        Permission::create(['name' => 'destroy']);

        //create roles and assign existing permissions
        $role1 = Role::create(['name' => 'admin']);
        $role1->givePermissionTo('index');
        $role1->givePermissionTo('create');
        $role1->givePermissionTo('store');
        $role1->givePermissionTo('show');
        $role1->givePermissionTo('edit');
        $role1->givePermissionTo('update');
        $role1->givePermissionTo('destroy');

        $role2 = Role::create(['name' => 'customer']);
        $role2->givePermissionTo('create');
        $role2->givePermissionTo('store');
        $role2->givePermissionTo('edit');
        $role2->givePermissionTo('update');

        $user = new App\User;
        $user->firstName = 'Anthony';
        $user->name = 'Lhermitte';
        $user->phoneNumber ='0680402856';
        $user->address ='215 Chemin de la Chalopinerie';
        $user->email ='anthony.lhermitte@yahoo.fr';
        $user->password = bcrypt('12345678');
        $user->save();
        $user->assignRole('$role1');
    
    }
}
