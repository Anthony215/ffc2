<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{

    public function run()
    {
        $role = Role::create(['name' => 'admin']);
        $role = Role::create(['name' => 'customer']);

        $user = new App\User;
        $user->firstName = 'Anthony';
        $user->name = 'Lhermitte';
        $user->phoneNumber ='0680402856';
        $user->address ='215 Chemin de la Chalopinerie';
        $user->email ='anthony.lhermitte@yahoo.fr';
        $user->password = bcrypt('12345678');
        $user->save();
        $user->assignRole('admin');

        $user = new App\User;
        $user->firstName = 'Sarah';
        $user->name = 'Porte';
        $user->phoneNumber ='0203040506';
        $user->address ='La Banque';
        $user->email ='sarah@mail.com';
        $user->password = bcrypt('12345678');
        $user->save();
        $user->assignRole('customer');

        $user = new App\User;
        $user->firstName = 'Harry';
        $user->name = 'Covert';
        $user->phoneNumber ='0203040506';
        $user->address ='Le Jardin';
        $user->email ='harry@mail.com';
        $user->password = bcrypt('12345678');
        $user->save();
        $user->assignRole('customer');

    }

}
