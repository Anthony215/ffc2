<?php

use Illuminate\Support\Facades\Route;
use Gloudemans\Shoppingcart\Facades\Cart;

// Route::get('/', function () {
//     return view('/home');
// });

// Auth::routes();

// // Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', 'HomeController@index')->name('home');

// Route::group ([ 'middleware' => [ 'auth' ]], function () {
//     // Route::resource ('roles', 'RoleController');
//     Route::resource('user', 'UserController');
// });
// // Route::get('/user/{user}/edit ', 'UserController@edit')->name('user.edit');//modif compte utilisateur
// // Route::put('/user/{user}', 'UserController@update')->name('user.update');

// Route::group(['middleware' => ['role:admin']], function() {
//     Route::get('/admin', function() {
//         return view('admin.homeadmin');
//     });
// });
// Route::group(['middleware' => ['role:customer']], function() {
//     Route::get('/customer', function() {
//         return view('home');
//     });
// });
// //Route errors
// Route::fallback(function()
// {
//     return view('error404');
// });
// /*------------------------------- Routes Panier---------------------*/
// /**
//  * products routes
//  */
// //Route::resource ('product', 'ProductController');
// Route::get('/product/{product}', 'ProductController@show')->name('product.show');
// Route::get('/burgersPage', 'ProductController@index');
// /**
//  * drinks routes
//  */
// Route::get('/drinksPage', 'DrinkController@index');
// /**
//  * cart routes
//  */
// Route::get('/cart', 'CartController@index')->name('cart.index');
// Route::post('/cart', 'CartController@store')->name('cart.store');
// Route::patch('/cart/{rowId}', 'CartController@update')->name('cart.update');
// Route::delete('/cart/{rowId}', 'CartController@destroy')->name('cart.destroy');

// //Route::delete('/cart/{product}', 'CartController@destroy')->name('destroyCart');

// Route::get('/emptycart', function() {
//     Cart::destroy();
// });


/////////////////////////////
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

//////test
Route::group(['middleware' => ['role:admin']], function() {
    Route::get('/admin', function() {
        return view('admin.homeadmin');
    });
});

// PRODUCTS vues sans connexion!----------------------à voir avec Olivier!

Route::get('menusPage', 'DrinkController@edit');//vue menu
Route::get('burgersPage', 'ProductController@index');//vue burger
Route::get('drinksPage', 'DrinkController@index');//vue boisson
Route::get('saucesPage', 'DrinkController@show');//vue sauces
Route::get('accPage', 'ProductController@show');//vue accompagnement
/////////////////////////////////////////////////////////////////////
//pour pouvoir executer sans être connecté:
Route::resource('cart', 'CartController');
Route::resource('user', 'UserController');
// ADMIN & CUSTOMER vues que si on est connecté!
Route::group(['middleware' => ['role:admin|customer']], function() {
    
    // Route::resource('product', 'ProductController')->only('index');
    Route::resource('product', 'ProductController');

});

// ADMIN
Route::group(['middleware' => ['role:admin']], function() {
    // Route::resource('product', 'ProductController')->except('index');
    Route::resource('product', 'ProductController');

   

});

Route::fallback(function()
{
    return view('home');
});
/////////////////////////////


