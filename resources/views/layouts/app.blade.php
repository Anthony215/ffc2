<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        @yield('extra-script')
        <!-- CSS front -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <link rel="stylesheet" href="css/home.css">
        <link rel="stylesheet" type="text/css" href="css/footer.css">

        <!-- script -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
    <div class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <div class="navbar-header">
                        <button class="navbar-toggle" data-target="#mobile_menu" data-toggle="collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                        <!-- <a class="navbar-brand">Fast Food Chic</a> -->
                    </div>

                    <div class="navbar-collapse collapse" id="mobile_menu">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/">Home</a></li>
                            <li class="active"><a href="menusPage">Menus</a></li>
                            <li class="active"><a href="burgersPage">Burgers</a></li>
                            <li class="active"><a href="accPage">Accompagnements</a></li>
                            <li class="active"><a href="saucesPage">Sauces</a></li>
                            <li class="active"><a href="drinksPage">Boissons</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('cart.index') }}"><span class="glyphicon glyphicon-shopping-cart"></span> Panier<span class="badge badge-pill bage-dark">{{ Cart::count() }}</span></a></li>


        
        @guest
        <li><a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Connexion<span class="caret"></span></a>                            
                                <ul class="dropdown-menu">
                                   <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-eye-open"></span> Se Connecter</a></li>
                                    @if(Route::has('register'))
                                    <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-folder-open"></span> Créer un compte</a></li>
                                    @endif
                                    @else
                                    <li><a href="user/{user}"><span class="glyphicon glyphicon-user"></span> Mon Compte<br>Bienvenue {{ Auth::user()->firstName }}</br></a></li>
                                    <li><a href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                   <span class="glyphicon glyphicon-eye-close"></span>Deconnexion
                                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"></a>
                                        @csrf
                                    </form></li>
                                </ul>
        </li>
        @endguest
        </ul>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-container">
    @yield('content')
    </div>
    @yield('extra-js')
    </body>
</html>