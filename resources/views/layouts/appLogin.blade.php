<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>

        <!-- CSS front -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
       
        <link rel="stylesheet" href="css/homelogin.css">
        <link rel="stylesheet" type="text/css" href="css/footer.css">
        
    </head>
    <body>
    
    @yield('content')
   
      
</html>