<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 
    @include('header')
</head>
<body>
@section('content')
<div class="container">
<div class="section-container">
            <div>
              @if (session('success'))
              <div class="alert alter-success" role="alert">
                {{ session('success') }}
        </div>
              @endif
    <div class="card-body">
    <table>
    <tr>
        <th>Id</th>
        <th>Prenom</th>
        <th>Nom</th>
        <th>Numéro de telephone</th>
        <th>Adresse</th>
        <th>Mail</th>
        <th>Mot de passe</th>
    </tr>
    @csrf
    
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->firstName }}</td>
        <td>{{ $user->name }}</td>
        <td>{{ $user->phoneNumber }}</td>
        <td>{{ $user->address }}</td>
        <td>{{ $user->email }}</td>
        <td>{{ $user->password_hash }}</td>
    </tr>  
</table>
                <form action="{{ route('user.update', $user->id) }}" method="PUT">
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Prenom') }}</i></span>
                        </div>
                        <input type="text" name="firstName" class="form-control" placeholder="{{ $user->firstName }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Nom') }}</i></span>
                        </div>
                        <input type="text" name="name" class="form-control" placeholder="{{ $user->name }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Telephone') }}</i></span>
                        </div>
                        <input type="text" name="phoneNumber" class="form-control" placeholder="{{ $user->phoneNumber }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Adresse') }}</i></span>
                        </div>
                        <input type="text" name="address" class="form-control" placeholder="{{ $user->address }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Email') }}</i></span>
                        </div>
                        <input type="text" name="email" class="form-control" placeholder="{{ $user->email }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i>{{ __('Mot de passe') }}</span>
                        </div>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="mot de passe">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user">{{ __('Confirmer mot de passe') }}</i></span>
                        </div>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirmer mot de passe">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Modifier" class="btn float-right login_btn">
                        <input type="reset" value="Annuler" onclick="location.href='/'" class="btn float-right cancel_btn">
                    </div>    

</div>

</body>
</html>
@include('layouts.footer')