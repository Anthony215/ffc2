

@extends('layouts.appadmin')
@section('content')
<div class="container">
    <div class="section-container">
            <div>
              @if (session('success'))
              <div class="alert alter-success" role="alert">
                {{ session('success') }}
        </div>
              @endif
    <div class="card-body">
    <table>
    <tr>
        <th>Prenom</th>
        <th>Nom</th>
        <th>Numéro de telephone</th>
        <th>Adresse</th>
        <th>Mail</th>
        <th>Mot de passe</th>
    </tr>
    @csrf
    <tr>
        <td>{{ Auth::user()->firstName }}</td>
        <td>{{ Auth::user()->name }}</td>
        <td>{{ Auth::user()->phoneNumber }}</td>
        <td>{{ Auth::user()->address }}</td>
        <td>{{ Auth::user()->email }}</td>
        <td>{{ Auth::user()->password_hash }}</td>
    </tr>

    <form action="{{ route('user.update', Auth::user()->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT')}}
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="firstName" class="form-control" placeholder="{{ Auth::user()->firstName }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="name" class="form-control" placeholder="{{ Auth::user()->name }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="phoneNumber" class="form-control" placeholder="{{ Auth::user()->phoneNumber }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="address" class="form-control" placeholder="{{ Auth::user()->address }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" name="email" class="form-control" placeholder="{{ Auth::user()->email }}">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="password" name="password" class="form-control" placeholder="mot de passe">
                    </div>
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirmer mot de passe">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Modifier" class="btn btn-danger">
                        <input type="reset" value="Annuler" onclick="location.href='/'" class="btn float-right cancel_btn">

                    </div>    

</div>  
    
</table>
                
@endsection
