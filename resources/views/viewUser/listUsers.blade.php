
    @extends('layouts.appadmin')
    @section('title')
Back office
@endsection

    @section('content')
    <div class="container">
    <div class="section-container">
            <div>
              @if (session('success'))
              <div class="alert alter-success" role="alert">
                {{ session('success') }}
        </div>
              @endif
<table>
    <tr>
        <th>Id</th>
        <th>Email</th>
    </tr>
    @foreach($user as $user)
    <tr>
        <td>{{ $user->id }}</td>
        <td>{{ $user->email }}</td>
        <td><form method="GET" action="{{ route('user.edit', $user->id) }}">
    @csrf
    @method('EDIT')
    <input class="btn btn-danger" type="submit" value="Modifier">
</form></td>
        <td><form method="POST" action="{{ route('user.destroy', $user->id) }}"
        onsubmit="return confirm('Etes vous sur de vouloir définitivement supprimer l\'entrée?');">
    @csrf
    @method('DELETE')
    <input class="btn btn-danger" type="submit" value="Supprimer">
</form></td>

</tr>
    @endforeach
</table>
</div>
@endsection
