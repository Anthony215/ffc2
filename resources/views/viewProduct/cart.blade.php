
@extends('layouts.appCart')

@section('title')
Panier
@endsection

@section('extra-script')
<script src="https://js.stripe.com/v3/"></script>
@endsection

@section('content')
@if(Cart::count() > 0)
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Produits</th>
                        <th>Détail</th>
                        <th class="text-center">Prix</th>
                        <!-- <th class="text-center">Total</th> -->
                        <th> </th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach(Cart::content() as $product)
                    <tr>
                        <td class="col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="card-img-top" src="{{ asset('img/'.$product->id) }}.jpg" alt="Burger"> </a>
                        </div></td>
                        
                        <td class="col-md-1" style="text-align: center">
                        <!-- <form action="{{ route('cart.update', $product->id) }}" method="PUT">
                        @csrf
                        @method('PUT')
                        
                        <div class="col m2 s12">
                        <input name="quantity" type="number" min="1" value="{{ $product->quantity }}">
                        </div>
                        </form> -->
                        

                        <div class="media-body">
                                <span class="text-warning"><strong>{{ $product->model->detail }}</strong></span>
                            </div>
                        </td>
                        <td class="col-md-1 text-center"><strong>{{ $product->model->price }}€</strong></td>

                        <!-- <td class="col-md-1 text-center"><strong>{{ number_format($product->quantity * $product->price) }} €</strong></td> -->

                        <td class="col-md-1">
                        
                            <form action="{{ route('cart.destroy', $product->rowId) }}" method="POST">
                                @csrf
                                @method('DELETE')

                                <button type="submit" class="btn btn-danger">Supprimer</button>
                            </form>
                        </button></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Nb article(s)</h5></td>
                        <td class="text-right"><h5><strong>{{ Cart::count() }}</strong></h5></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong>{{ Cart::subtotal() }}€</strong></h3></td>
                    </tr>
                    
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart" id="continue"></span> se faire livrer
                        </button></td>
                        <td>
                        <button type="button" class="btn btn-success">
                            à emporter <span class="glyphicon glyphicon-play"></span>
                        </button></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="col-sm-12 col-md-10 col-md-offset-1">
<div class="empty-cart">
<div class="alert alert-warning" role="alert">
    <p>Votre panier est vide!</p>
</div>

</div>
</div>
@endif
@endsection

@section('javascript')
<script>
    document.addEventListener('DOMContentLoaded', () => {
      const quantities = document.querySelectorAll('input[name="quantity"]');
      quantities.forEach( input => {
        input.addEventListener('input', e => {
          if(e.target.value < 1) {
            e.target.value = 1;
          } else {
            e.target.parentNode.parentNode.submit();
            document.querySelector('#wrapper').classList.add('hide');
            document.querySelector('#action').classList.add('hide');
            document.querySelector('#loader').classList.remove('hide');
          }
        });
      }); 
      const deletes = document.querySelectorAll('.deleteItem');
      deletes.forEach( icon => {
        icon.addEventListener('click', e => {
          e.target.parentNode.parentNode.submit();
          document.querySelector('#wrapper').classList.add('hide');
          document.querySelector('#loader').classList.remove('hide');
        });
      }); 
    });
</script>
@endsection
@section('javascript')
<script>
document.addEventListener('DOMContentLoaded', () => {
    const quantities = document.querySelectorAll('input[name="quantity"]');
    quantities.forEach( input => {
        input.addEventListener('input', e => {
            if(e.target.value <1) {
                e.target.value = 1;
            }else {
                e.target.parentNode.parentNode.submit();
                document.querySelector('#wrapper').classList.add('hide');
                document.querySelector('#action').classList.add('hide');
                document.querySelector('#loader').classList.remove('hide');
            }
        });
    });
    const deletes = document.querySelectorAll('.deleteItem');
    deletes.forEach(icon => {
        icon.addEventListener('click'), e => {
            e.target.parentNode.parentNode.submit();
            document.querySelector('#wrapper').classList.add('hide');
            document.querySelector('#loader').classList.remove('hide');
        });
    });
});
</script>
@endsection

<!-- @include('layouts.footer') -->