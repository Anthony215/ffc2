@extends('layouts.appCart')


@section('content')
<div role="main">
<div class="album py-5 bg-light">
        <div class="container">
          <div class="section-container">
            <div>
              @if (session('success'))
              <div class="alert alter-success" role="alert">
                {{ session('success') }}
        </div>
              @endif

              @if(count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
                </ul>
              </div>
              @endif
          <div class="row">
          @foreach($products as $product)
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="/img/{{ $product->id }}.jpg" alt="produit">
                <!-- <img class="card-img-top" src="/img/1.jpg" alt="Burger"> -->
                <div class="card-body">
                  <p class="card-text">{{ $product->name }}</p>
                  <p class="card-text">{{ $product->detail }}</p>
                  <p class="card-text">{{ $product->price }}€</p>
                </div>
              </div>
            </div>
          @endforeach  
        </div>
      </div>
</div>
</div>
@endsection
</body>
</html>
<!-- @include('layouts.footer') -->