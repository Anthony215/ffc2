@extends('layouts.appCart')


@section('content')
<div role="main">
<div class="album py-5 bg-light">
        <div class="container">
          <div class="section-container">
            <div>
              @if (session('success'))
              <div class="alert alter-success" role="alert">
                {{ session('success') }}
        </div>
              @endif

              @if(count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
                </ul>
              </div>
              @endif
          <div class="row">
          @foreach($products as $product)
            <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" src="{{ asset('img/'.$product->id) }}.jpg" alt="produit">
                <!-- <img class="card-img-top" src="/img/{{ $product->id }}.jpg" alt="produit"> -->
                <div class="card-body">
                  <p class="card-text">{{ $product->name }}</p>
                  <p class="card-text">{{ $product->detail }}</p>
                  <p class="card-text">{{ $product->price }}€</p>
                  <div class="d-flex justify-content-between align-items-center">
                   <form action="{{ route('cart.store') }}" method="POST">
                   @csrf
                   <input type="hidden" name="id" value="{{ $product->id }}">
                   <input type="hidden" name="name" value="{{ $product->name }}">
                   <input type="hidden" name="price" value="{{ $product->price }}">
                   <button type="submit" class="btn btn-primary">Ajouter au panier</button>
                   </form>
                  </div>
                </div>
              </div>
            </div>
          @endforeach  
        </div>
      </div>
</div>
</div>
@endsection
</body>
</html>
<!-- @include('layouts.footer') -->